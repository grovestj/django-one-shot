from django.shortcuts import render
from todos.models import TodoList


# Create your views here.
def todo_list_list(request):
    full_lst = TodoList.objects.all()
    context = {
        "todo_list_list": full_lst,
    }
    return render(request, "todos/list.html", context)
